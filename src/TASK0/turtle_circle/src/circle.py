#!/usr/bin/env python

import rospy
from geometry_msgs.msg import Twist
from turtlesim.msg import Pose

PI = 3.1415926535897

def update_pose(data):
	print "x is :",data.x,"y is :",data.y
	#prints x and y ##also containes theta, linear/anular velocity

def handle_move_circle():
	pub = rospy.Publisher('/turtle1/cmd_vel',Twist, queue_size = 10) #create publish
	pose_subscriber = rospy.Subscriber('/turtle1/pose',Pose, update_pose) #create subscriber
	rate = rospy.Rate(50) #sampeling rate
	vel_msg = Twist()
	speed = 1 #message from service
	radius = 1

	vel_msg.linear.x = speed
	vel_msg.linear.y = 0
	vel_msg.linear.z = 0
	vel_msg.angular.x = 0
	vel_msg.angular.y = 0
	vel_msg.angular.z = speed/radius

	t0 = rospy.Time.now().to_sec() #initial time
	current_dist = 0.0
	dist = 2*PI*radius #total distance

	#Move Robot in circle
	while (current_dist<dist):
		rospy.loginfo("moving in circle")
		pub.publish(vel_msg) #publish message
		t1=rospy.Time.now().to_sec() #final time
		current_dist= speed*(t1-t0) #distance is speed * time taken
		print(current_dist)
		rate.sleep()

	print("goal achived")
	vel_msg.linear.x = 0 #stop turtle
	vel_msg.linear.z = 0
	pub.publish(vel_msg)	

def move_circle_server():
	rospy.init_node('move_circle_server')
	rospy.sleep(2)
	handle_move_circle()

if __name__ == "__main__":
	move_circle_server()
