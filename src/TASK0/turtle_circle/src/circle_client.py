#!/usr/bin/env python

import rospy
from turtle_circle.srv import *

def move_circle_client():
    rospy.wait_for_service('move_circle') #waits for service to be avalible
    try:
        move_circle = rospy.ServiceProxy('move_circle', MoveCircle) #request service message
        move_circle(1,1)	#speed,radius
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e

if __name__ == "__main__":
    move_circle_client()
